(function($) {

/**
* Behavior for the jRotate Drupal module.
*/
Drupal.behaviors.jrotate = {
  attach: function(context, settings) {
    var angle = 0;
    var selector = settings.jrotate.selector;
    setInterval(function() {
      if (angle > 360) angle = 0;
      angle+=settings.jrotate.style.angle;
      $(selector).rotate(angle);
    },50);
  }
};

})(jQuery);
