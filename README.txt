DESCRIPTION
===========

Integrate the jqueryrotate plugin into Drupal

CONFIGURATION
=============

* Go to the configuration screen at 
Administration >> Configuration >> Media >> jRotate.

Selector
--------

* Enter your own jQuery selectors. Use a new line for each selector.

Style
-----
Right now there is only one, 360 which just rotates the element selected over 360 degrees.
